package com.livekeys.officetool.pptutil.entity;

import com.alibaba.fastjson.JSONArray;

import java.util.List;

/**
 * table表格入参
 */
public class TableParam {
    /** 表格列 */
    private Integer colNum;
    private String title;
    private List<String> titleList;
    private List<JSONArray> tableData;

    public TableParam(Integer colNum, String title, List<String> titleList, List<JSONArray> tableData) {
        this.colNum = colNum;
        this.title = title;
        this.titleList = titleList;
        this.tableData = tableData;
    }

    public Integer getColNum() {
        return colNum;
    }

    public void setColNum(Integer colNum) {
        this.colNum = colNum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<String> titleList) {
        this.titleList = titleList;
    }

    public List<JSONArray> getTableData() {
        return tableData;
    }

    public void setTableData(List<JSONArray> tableData) {
        this.tableData = tableData;
    }

    @Override
    public String toString() {
        return "TableParam{" +
                "colNum=" + colNum +
                ", title='" + title + '\'' +
                ", titleList=" + titleList +
                ", tableData=" + tableData +
                '}';
    }
}
