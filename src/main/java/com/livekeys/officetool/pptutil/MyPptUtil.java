package com.livekeys.officetool.pptutil;

import org.apache.poi.sl.usermodel.TextBox;
import org.apache.poi.xslf.usermodel.*;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.main.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ppt工具类
 */
public class MyPptUtil {
    /**
     * 日志
     */
    private static final Logger logger = LoggerFactory.getLogger(MyPptUtil.class);

    /**
     * ppt对象
     */
    private XMLSlideShow pptx;

    /**
     * 构造方法
     *
     * @param filePath - ppt模板路径
     */
    public MyPptUtil(String filePath) {
        this.readPpt(filePath);
    }

    /**
     * 获取ppt对象
     *
     * @return {@link XMLSlideShow} - ppt对象
     */
    public XMLSlideShow getPpt() {
        return pptx;
    }

    /**
     * 读取 ppt
     *
     * @param filePath ppt模板路径
     * @return {@link XMLSlideShow} - ppt对象
     */
    private XMLSlideShow readPpt(String filePath) {
        try {
            this.pptx = new XMLSlideShow(new FileInputStream(filePath));
            if (logger.isDebugEnabled()) {
                logger.debug("已读取文件：" + filePath);
            }
            return this.pptx;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 写入 ppt
     *
     * @param exportPath - ppt导出路径
     */
    public void writePpt(String exportPath) {
        try {
            File file = new File(exportPath);
            if (file.exists()) {
                file.delete();
            }
            this.pptx.write(new FileOutputStream(exportPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取所有幻灯片
     *
     * @return List<XSLFSlide> - 幻灯片列表
     */
    public List<XSLFSlide> getSlides() {
        return pptx.getSlides();
    }

    /**
     * 获取所有幻灯片内的所有段落
     *
     * @return List<XSLFTextParagraph> - 段落列表
     */
    public List<XSLFTextParagraph> getParagraphsAll() {
        List<XSLFTextParagraph> xslfTextParagraphList = new ArrayList<>();
        for (XSLFSlide slide : getSlides()) {
            xslfTextParagraphList.addAll(getParagraphsFromSlide(slide));
        }
        return xslfTextParagraphList;
    }

    /**
     * 获取指定幻灯片内的所有段落
     *
     * @param slide - ppt页对象
     * @return List<XSLFTextParagraph> - 段落列表
     */
    public List<XSLFTextParagraph> getParagraphsFromSlide(XSLFSlide slide) {
        // 存放所有 shape 的所有段落
        List<XSLFTextParagraph> textParagraphs = new ArrayList<>();
        // 存放所有可能拥有段落文本的 shape
        List<XSLFShape> textShapes = new ArrayList<>();
        // 解析出所有段落文本的 shape
        List<XSLFShape> shapes = slide.getShapes();
        for (XSLFShape shape : shapes) {
            getTextShape(textShapes, shape);
        }
        // 解析出所有段落
        for (XSLFShape shape : textShapes) {
            textParagraphs.addAll(parseParagraph(shape));
        }
        return textParagraphs;
    }

    /**
     * 解析出所有可能拥有段落文本的 shape
     *
     * @param shapeList - 存放shape的列表
     * @param shape     - shape
     */
    private void getTextShape(List<XSLFShape> shapeList, XSLFShape shape) {
        if (shape instanceof XSLFGroupShape) {
            XSLFGroupShape groupShape = (XSLFGroupShape) shape;
            for (XSLFShape xslfShape : groupShape.getShapes()) {
                getTextShape(shapeList, xslfShape);
            }
        } else {
            shapeList.add(shape);
        }
    }

    /**
     * 解析一个 shape 内的所有段落
     *
     * @param shape - shape
     * @return List<XSLFTextParagraph> - 段落列表
     */
    private List<XSLFTextParagraph> parseParagraph(XSLFShape shape) {
        if (shape instanceof XSLFAutoShape) {
            XSLFAutoShape autoShape = (XSLFAutoShape) shape;
            return autoShape.getTextParagraphs();
        } else if (shape instanceof XSLFTextShape) {
            XSLFTextShape textShape = (XSLFTextShape) shape;
            return textShape.getTextParagraphs();
        } else if (shape instanceof XSLFFreeformShape) {
            XSLFFreeformShape freeformShape = (XSLFFreeformShape) shape;
            return freeformShape.getTextParagraphs();
        } else if (shape instanceof TextBox) {
            TextBox textBox = (TextBox) shape;
            return textBox.getTextParagraphs();
        }
        return new ArrayList<>();
    }

    /**
     * 替换段内的标签文本
     *
     * @param paragraph - 文字段落
     * @param paramMap  - 要替换的参数Map集合
     */
    public void replaceTagInParagraph(XSLFTextParagraph paragraph, Map<String, Object> paramMap) {
        String paraText = paragraph.getText();
        String regEx = "\\$\\{(.*?)}";
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(paraText);

        if (matcher.find()) {
            // StringBuilder keyWord = new StringBuilder();
            int start = getRunIndex(paragraph, "${");
            int end = getRunIndex(paragraph, "}");

            // 处理 ${***} 在一个 run 内的情况
            if (start == end) {
                // 存放标签
                String rs = matcher.group(0);
                // 存放 key
                // keyWord.append(rs.replace("${", "").replace("}", ""));
                // 获取标签所在 run 的全部文字
                String text = getRunsT(paragraph, start, end + 1);
                // 如果没在 paramMap 中没有找到这个标签所对应的值，那么就直接替换成标签的值
                // String v = nullToDefault(paramMap.get(keyWord.toString()), keyWord.toString());
                String v = nullToDefault(paramMap.get(matcher.group(1)), matcher.group(1));
                // 重新设置文本
                setText(paragraph.getTextRuns().get(start), text.replace(rs, v));
            } else {
                /*String rs = matcher.group(0);
                String rs1 = matcher.group(1);*/
                // 获取标签所在 run 的全部文字
                // String text = getRunsT(paragraph, start, end + 1);
                String v = nullToDefault(paramMap.get(matcher.group(1)), matcher.group(1));
                // setText(paragraph.getTextRuns().get(start), text.replace(rs, v));
                setText(paragraph.getTextRuns().get(start), "");
                setText(paragraph.getTextRuns().get(start + 1), v);
                for (int i = start + 2; i <= end; i++) {
                    setText(paragraph.getTextRuns().get(i), "");
                }

                System.out.println(v);
            }
            // 继续找
            replaceTagInParagraph(paragraph, paramMap);
        }
    }

    /**
     * 设置 run 的值
     *
     * @param run - run对象
     * @param t   - 新文字
     */
    private void setText(XSLFTextRun run, String t) {
        run.setText(t);
    }

    // 获取 word 在段落中出现第一次的 run 的索引
    private int getRunIndex(XSLFTextParagraph paragraph, String word) {
        List<CTRegularTextRun> rList = paragraph.getXmlObject().getRList();
        for (int i = 0; i < rList.size(); i++) {

            String text = rList.get(i).getT();
            if (text.contains(word)) {
                return i;
            }
        }
        return -1;
    }

    // 获取段落下特定索引的 run 的值
    private String getRunsT(XSLFTextParagraph paragraph, int start, int end) {
        List<XSLFTextRun> textRuns = paragraph.getTextRuns();
        StringBuilder t = new StringBuilder();
        for (int i = start; i < end; i++) {
            t.append(textRuns.get(i).getRawText());
        }
        return t.toString();
    }


    /**
     * 从所有幻灯片中获取表格列表
     *
     * @return List<XSLFTable> - 所有表格列表
     */
    public List<XSLFTable> getAllTableAll() {
        List<XSLFTable> xslfTableList = new ArrayList<>();
        for (XSLFSlide slide : getSlides()) {
            xslfTableList.addAll(getAllTableFromSlide(slide));
        }
        return xslfTableList;
    }

    /**
     * 从幻灯片中获取表格列表
     *
     * @param slide - ppt页对象
     * @return List<XSLFTable> - 表格列表
     */
    public List<XSLFTable> getAllTableFromSlide(XSLFSlide slide) {
        List<XSLFTable> tables = new ArrayList<>();
        for (XSLFShape shape : slide.getShapes()) {
            if (shape instanceof XSLFTable) {
                tables.add((XSLFTable) shape);
            }
        }
        return tables;
    }

    /**
     * 获取表格的行数
     *
     * @param table - 表格
     * @return int - 表格总行数
     */
    public int getTableRowsNum(XSLFTable table) {
        return table.getNumberOfRows();
    }

    /**
     * 获取表格的列数
     *
     * @param table - 表格
     * @return int - 列数
     */
    public int getTableColsNum(XSLFTable table) {
        return table.getNumberOfColumns();
    }


    /**
     * 获取表格某行
     *
     * @param table  - table对象
     * @param rowNum - 行数
     * @return XSLFTableRow - 行对象
     */
    public XSLFTableRow getTableRow(XSLFTable table, int rowNum) {
        return table.getRows().get(rowNum);
    }

    /**
     * 获取行列表
     *
     * @param table - 表格对象
     * @return List<XSLFTableRow> - 行列表
     */
    public List<XSLFTableRow> getTableRows(XSLFTable table) {
        return table.getRows();
    }

    /**
     * 根据某行获取单元格列表
     *
     * @param row - 行对象
     * @return XSLFTableCell - 列
     */
    public XSLFTableCell getTableCol(XSLFTableRow row, int rowNum) {
        return getTableCols(row).get(rowNum);
    }

    /**
     * 根据某行获取单元格列表
     *
     * @param row - 行对象
     * @return List<XSLFTableCell> - 列列表
     */
    public List<XSLFTableCell> getTableCols(XSLFTableRow row) {
        return row.getCells();
    }


    /**
     * 获取边框中某方向上的属性
     *
     * @param cellProperties - 单元格属性
     * @param posStr         - posStr
     * @return CTLineProperties - 线条属性
     */
    private CTLineProperties getCTLineProperties(CTTableCellProperties cellProperties, String posStr) {
        // 定义边框下的方向
        /*List<String> posList = new ArrayList<>();
        posList.addAll(Arrays.asList("top", "left", "bottom", "right"));*/

        switch (posStr) {
            case "left":
                return cellProperties.isSetLnL() ? cellProperties.getLnL() : cellProperties.addNewLnL();
            case "right":
                return cellProperties.isSetLnR() ? cellProperties.getLnR() : cellProperties.addNewLnR();
            case "top":
                return cellProperties.isSetLnT() ? cellProperties.getLnT() : cellProperties.addNewLnT();
            case "bottom":
                return cellProperties.isSetLnB() ? cellProperties.getLnB() : cellProperties.addNewLnB();
            default: {
                logger.warn(posStr + " position not exists! position include ['left', 'right', 'top', 'bottom'");
                return cellProperties.isSetLnL() ? cellProperties.getLnT() : cellProperties.addNewLnT();
            }
        }
    }


    /**
     * 设置单元格文本
     *
     * @param cell - 单元格
     * @param text - 要设置的文本
     */
    public void setCellText(XSLFTableCell cell, String text) {
        // cell.setText(text);
        CTTableCell ctTableCell = (CTTableCell) cell.getXmlObject();
        CTTextBody ctTextBody = ctTableCell.isSetTxBody() ? ctTableCell.getTxBody() : ctTableCell.addNewTxBody();
        CTTextParagraph p;
        if (ctTextBody.getPList().size() < 1) {
            p = ctTextBody.addNewP();
        } else {
            p = ctTextBody.getPList().get(0);
            // 删除多余的 p
            if (ctTextBody.getPList().size() > 1) {
                for (int i = 1; i < ctTextBody.getPList().size(); i++) {
                    ctTextBody.removeP(i);
                }
            }
        }

        CTRegularTextRun r;
        if (p.getRList().size() < 1) {
            r = p.addNewR();
        } else {
            r = p.getRList().get(0);
            // 删除多余的 r
            if (p.getRList().size() > 1) {
                for (int i = 1; i < p.getRList().size(); i++) {
                    p.removeR(i);
                }
            }
        }
        // 设置文本
        r.setT(text);
    }


    /**
     * 设置单元格水平对齐方式
     * @param cell
     * @param horizontalAlign
     */
    public void setCellHorizontalAlign(XSLFTableCell cell, String horizontalAlign) {
        List<XSLFTextParagraph> textParagraphs = cell.getTextParagraphs();
        for (XSLFTextParagraph textParagraph : textParagraphs) {
            setParagraphHorizontalAlign(textParagraph, horizontalAlign);
        }
    }


    /**
     * 设置幻灯片段落水平对齐方式
     * @param paragraph
     * @param horizontal
     */
    public void setParagraphHorizontalAlign(XSLFTextParagraph paragraph, String horizontal) {
        horizontal = this.nullToDefault(horizontal, "auto");

        setCTTextParagraphHorizonAlign(paragraph, horizontal.toLowerCase());
    }

    // 设置段落水平对齐方式
    private void setCTTextParagraphHorizonAlign(XSLFTextParagraph ctTextParagraph, String horizontalStr) {
        CTTextParagraphProperties pPr = this.getPPR(ctTextParagraph);

        switch (horizontalStr) {
            case "left" : pPr.setAlgn(STTextAlignType.L);   break;  // 左对齐
            case "right": pPr.setAlgn(STTextAlignType.R);   break;  // 右对齐
            case "center": pPr.setAlgn(STTextAlignType.CTR);    break;  // 居中
            case "disperse" : pPr.setAlgn(STTextAlignType.DIST);    break;  // 分散对齐
            default: pPr.setAlgn(STTextAlignType.JUST); // 两端对齐
        }
    }

    /**
     * 设置单元格垂直对齐方式
     * @param cell
     * @param verticalAlign
     */
    public void setCellVerticalAlign(XSLFTableCell cell, String verticalAlign) {
        List<XSLFTextParagraph> textParagraphs = cell.getTextParagraphs();
        for (XSLFTextParagraph textParagraph : textParagraphs) {
            setParagraphVerticalAlign(textParagraph, verticalAlign);
        }
    }


    /**
     * 设置幻灯片段落垂直对齐方式
     * @param paragraph
     * @param vertical
     */
    public void setParagraphVerticalAlign(XSLFTextParagraph paragraph, String vertical) {
        vertical = this.nullToDefault(vertical, "auto");

        setCTTextParagraphVerticalAlign(paragraph, vertical.toLowerCase());
    }


    // 设置段落垂直对齐
    private void setCTTextParagraphVerticalAlign(XSLFTextParagraph ctTextParagraph, String verticalStr) {
        CTTextParagraphProperties pPr = this.getPPR(ctTextParagraph);
        switch (verticalStr) {
            case "top" : pPr.setFontAlgn(STTextFontAlignType.T);   break;   // 顶部
            case "baseline" : pPr.setFontAlgn(STTextFontAlignType.BASE);   break;  // 基线对齐
            case "bottom" : pPr.setFontAlgn(STTextFontAlignType.B);   break;    // 底部
            case "center" : pPr.setFontAlgn(STTextFontAlignType.CTR);   break;    // 居中
            default: pPr.setFontAlgn(STTextFontAlignType.AUTO);  // 自动
        }
    }


    // 获取 pPr
    private CTTextParagraphProperties getPPR(XSLFTextParagraph ctTextParagraph) {
        return ctTextParagraph.getXmlObject().getPPr() == null ? ctTextParagraph.getXmlObject().addNewPPr() : ctTextParagraph.getXmlObject().getPPr();
    }

    /**
     * 设置该单元格所有的边框颜色和线条
     *
     * @param cell         - 单元格
     * @param lineType     - 线条样式
     * @param lineColorHex - 高亮颜色
     */
    public void setCellBorder(XSLFTableCell cell, String lineType, String lineColorHex) {
        CTTableCellProperties cellProperties = getCellProperties(cell);
        if (lineType == null || "".equals(lineType)) {
            cellProperties.unsetLnT();
            cellProperties.unsetLnB();
            cellProperties.unsetLnR();
            cellProperties.unsetLnL();
        } else {
            setCellBorder(getCTLineProperties(cellProperties, "left"), lineType, lineColorHex);
            setCellBorder(getCTLineProperties(cellProperties, "right"), lineType, lineColorHex);
            setCellBorder(getCTLineProperties(cellProperties, "top"), lineType, lineColorHex);
            setCellBorder(getCTLineProperties(cellProperties, "bottom"), lineType, lineColorHex);
        }
    }

    /**
     * 设置边框的颜色和线条类型
     *
     * @param ctLineProperties - 方向
     * @param lineType         - 线条类型
     * @param lineColorHex     - 高亮颜色
     */
    private void setCellBorder(CTLineProperties ctLineProperties, String lineType, String lineColorHex) {
        // 构造边框线的类型
        List<String> lineTypes =
                new ArrayList<>(Arrays.asList("solid", "dot", "dash", "lgDash", "dashDot", "lgDashDot",
                        "lgDashDotDot", "sysDash", "sysDot", "sysDashDot", "sysDashDotDot"));

        // 设置边框线的颜色
        CTSolidColorFillProperties ctSolidColorFillProperties = ctLineProperties.addNewSolidFill();
        CTSRgbColor ctsRgbColor = ctSolidColorFillProperties.addNewSrgbClr();
        ctsRgbColor.setVal(hexToByteArray(lineColorHex.substring(1)));

        CTPresetLineDashProperties ctPresetLineDashProperties = ctLineProperties.addNewPrstDash();
        if (lineTypes.contains(lineType)) {
            // 在 lineTypes 里面
            ctPresetLineDashProperties.setVal(STPresetLineDashVal.Enum.forString(lineType));
        } else {
            // 没在 lineTyppes 里面
            ctPresetLineDashProperties.setVal(STPresetLineDashVal.Enum.forString("solid"));
        }
    }

    /**
     * 根据 XSLFTableCell 获取 TcPr
     *
     * @param cell - 单元格
     * @return CTTableCellProperties 返回TcPr
     */
    private CTTableCellProperties getCellProperties(XSLFTableCell cell) {
        CTTableCell ctTableCell = (CTTableCell) cell.getXmlObject();
        return ctTableCell.isSetTcPr() ? ctTableCell.getTcPr() : ctTableCell.addNewTcPr();
    }

    /**
     * 设置中文字体
     *
     * @param run        - run对象
     * @param fontFamily - 字体
     * @return XSLFTextRun - 返回对象
     */
    public XSLFTextRun setTextFontFamily(XSLFTextRun run, String fontFamily) {
        if (fontFamily == null || "".equals(fontFamily)) {
            return run;
        }

        CTTextCharacterProperties rPr = getRPr(run.getXmlObject());
        this.setRPRChineseFontFamily(rPr, fontFamily);
        return run;
    }

    /**
     * 设置字体加粗
     *
     * @param run  - run对象
     * @param bold - 是否加粗
     * @return XSLFTextRun - 对象
     */
    public XSLFTextRun setTextBold(XSLFTextRun run, Boolean bold) {
        if (bold == null) {
            return run;
        }

        run.setBold(bold);
        return run;
    }

    /**
     * 设置 cell 的颜色
     *
     * @param cell     - 单元格
     * @param colorHex - 高亮颜色
     */
    public void setCellColor(XSLFTableCell cell, String colorHex) {
        List<XSLFTextParagraph> textParagraphs = cell.getTextParagraphs();
        for (XSLFTextParagraph textParagraph : textParagraphs) {
            for (XSLFTextRun textRun : textParagraph.getTextRuns()) {
                setTextColor(textRun, colorHex);
            }
        }
    }

    /**
     * 设置 cell 文本是否加粗
     *
     * @param cell - 单元格
     * @param bold - 是否加粗
     */
    public void setCellBold(XSLFTableCell cell, Boolean bold) {
        List<XSLFTextParagraph> textParagraphs = cell.getTextParagraphs();
        for (XSLFTextParagraph textParagraph : textParagraphs) {
            for (XSLFTextRun textRun : textParagraph.getTextRuns()) {
                setTextBold(textRun, bold);
            }
        }
    }


    /**
     * 设置单元格中文字体
     *
     * @param cell       - 单元格
     * @param fontFamily - 字体
     */
    public void setCellFontfamily(XSLFTableCell cell, String fontFamily) {
        List<XSLFTextParagraph> textParagraphs = cell.getTextParagraphs();
        for (XSLFTextParagraph textParagraph : textParagraphs) {
            for (XSLFTextRun textRun : textParagraph.getTextRuns()) {
                setTextFontFamily(textRun, fontFamily);
            }
        }
    }

    /**
     * 设置单元格文本字体大小
     *
     * @param cell     - 单元格
     * @param fontSize - 字体大小
     */
    public void setCellFontSize(XSLFTableCell cell, String fontSize) {
        List<XSLFTextParagraph> textParagraphs = cell.getTextParagraphs();
        for (XSLFTextParagraph textParagraph : textParagraphs) {
            for (XSLFTextRun textRun : textParagraph.getTextRuns()) {
                setTextFontSize(textRun, fontSize);
            }
        }
    }

    /**
     * 设置字体大小
     *
     * @param run      - run对象
     * @param fontSize - 字体大小
     * @return XSLFTextRun - run对象
     */
    public XSLFTextRun setTextFontSize(XSLFTextRun run, String fontSize) {
        if (fontSize == null || "".equals(fontSize)) {
            return run;
        }
        // 设置字体大小
        run.setFontSize(Double.valueOf(fontSize));
        return run;
    }

    /**
     * 设置字体颜色
     *
     * @param run      - run对象
     * @param colorHex 16进制颜色单位
     * @return XSLFTextRun - run对象
     */
    public XSLFTextRun setTextColor(XSLFTextRun run, String colorHex) {
        if (colorHex == null || "".equals(colorHex)) {
            return run;
        }

        CTTextCharacterProperties rPr = getRPr(run.getXmlObject());
        CTSolidColorFillProperties solidColor = rPr.isSetSolidFill() ? rPr.getSolidFill() : rPr.addNewSolidFill();
        CTSRgbColor ctColor = solidColor.isSetSrgbClr() ? solidColor.getSrgbClr() : solidColor.addNewSrgbClr();
        ctColor.setVal(hexToByteArray(colorHex.substring(1)));
        return run;
    }

    /**
     * 设置中文字体
     *
     * @param rPr        -
     * @param fontFamily - 字体
     */
    private void setRPRChineseFontFamily(CTTextCharacterProperties rPr, String fontFamily) {
        if (fontFamily == null || "".equals(fontFamily)) {
            fontFamily = "宋体";
        }

        if (rPr.isSetLatin()) {
            rPr.unsetLatin();
        }

        CTTextFont ea = rPr.isSetEa() ? rPr.getEa() : rPr.addNewEa();
        ea.setTypeface(fontFamily);
        ea.setPitchFamily(new Byte("34"));
        ea.setCharset(new Byte("-122"));

        CTTextFont cs = rPr.isSetCs() ? rPr.getCs() : rPr.addNewCs();
        cs.setTypeface(fontFamily);
        cs.setPitchFamily(new Byte("34"));
        cs.setCharset(new Byte("-122"));
    }

    /**
     * 获取 rPR
     *
     * @param xmlObject - 操作对象
     * @return CTTextCharacterProperties 属性
     */
    private CTTextCharacterProperties getRPr(XmlObject xmlObject) {
        if (xmlObject instanceof CTTextField) {
            CTTextField tf = (CTTextField) xmlObject;
            return tf.getRPr() == null ? tf.addNewRPr() : tf.getRPr();
        } else if (xmlObject instanceof CTTextLineBreak) {
            CTTextLineBreak tlb = (CTTextLineBreak) xmlObject;
            return tlb.getRPr() == null ? tlb.addNewRPr() : tlb.getRPr();
        } else {
            CTRegularTextRun tr = (CTRegularTextRun) xmlObject;
            return tr.getRPr() == null ? tr.addNewRPr() : tr.getRPr();
        }
    }

    /**
     * 空字符串转默认值
     *
     * @param goalStr    - 要转换的字符串
     * @param defaultStr - 默认字符串
     * @return String - 转换后的字符串
     */
    private String nullToDefault(String goalStr, String defaultStr) {
        if (goalStr == null || "".equals(goalStr)) {
            return defaultStr;
        }
        return goalStr;
    }

    /**
     * 空字符串转默认值
     *
     * @param o          - 要转换的对象
     * @param defaultStr - 默认字符串
     * @return String - 转换后的字符串
     */
    private String nullToDefault(Object o, String defaultStr) {
        if (o == null) {
            return defaultStr;
        } else {
            if ("".equals(o.toString())) {
                return defaultStr;
            } else {
                return o.toString();
            }
        }
    }

    /**
     * 将16进制转换为 byte 数组
     *
     * @param inHex 需要转换的字符串
     * @return byte[] - 数组
     */
    public byte[] hexToByteArray(String inHex) {
        int hexlen = inHex.length();
        byte[] result;
        if (hexlen % 2 == 1) {
            // 奇数的话，就在前面添加 0
            hexlen++;
            result = new byte[(hexlen / 2)];
            inHex = "0" + inHex;
        } else {
            // 偶数
            result = new byte[(hexlen / 2)];
        }
        int j = 0;
        for (int i = 0; i < hexlen; i += 2) {
            result[j] = this.hexToByte(inHex.substring(i, i + 2));
            j++;
        }
        return result;
    }

    private byte hexToByte(String inHex) {
        return (byte) Integer.parseInt(inHex, 16);
    }

    /**
     * 判断JsonArray数据类型
     * @param obj - 数据
     * @return 字符串值
     */
    public static String getValueFromJsonArray(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        } else if (obj instanceof Integer){
            return String.valueOf(obj);
        } else if (obj instanceof BigDecimal) {
            BigDecimal bigDecimal = (BigDecimal) obj;
            return String.valueOf(bigDecimal);
        }
        return null;
    }

}
