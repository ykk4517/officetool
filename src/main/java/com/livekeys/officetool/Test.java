package com.livekeys.officetool;

import com.alibaba.fastjson.JSONArray;
import com.livekeys.officetool.pptutil.PptUtil;
import com.livekeys.officetool.pptutil.entity.TableParam;
import org.apache.poi.sl.usermodel.TableCell;
import org.apache.poi.sl.usermodel.VerticalAlignment;
import org.apache.poi.xslf.usermodel.*;
import org.apache.xmlbeans.XmlException;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description TODO
 * @Author YKK
 * @Date 2021/5/8 21:45
 **/

public class Test {
    private static String filePath = "C:\\Users\\YKK\\Desktop\\个人简历.pptx";
    //private static String filePath = "C:\\Users\\YKK\\Desktop\\个人简历.ppt";
    private static PptUtil pptUtil;

    public static void main(String[] args) throws IOException, XmlException {
        pptUtil = new PptUtil(filePath);
        Map<String, Object> allData = setParamters();
        System.out.println("allData = " + allData);
        // testText((Map<String, Object>) allData.get("textMap"), true);
        testTable((Map<String, TableParam>) allData.get("tableMap"));

        pptUtil.writePpt("C:\\Users\\YKK\\Desktop\\公司简介1.pptx");
    }

    /**
     * 初始化，模拟入参
     *
     * @return Map<String, Object> - 入参集合
     */
    public static Map<String, Object> setParamters() {
        Map<String, Object> allData = new HashMap<>();

        // 构造数据
        Map<String, Object> keyMap = new HashMap<>();
        keyMap.put("date", "2021.05");
        keyMap.put("name", "张三");
        keyMap.put("age", 18);
        keyMap.put("school", "测试大学");
        keyMap.put("major", "软件专业");
        keyMap.put("address", "建设路188号");
        keyMap.put("phone", "123521");

        allData.put("textMap", keyMap);


        Map<String, TableParam> tableParamMap = new HashMap<>();

        List<String> titleList = Arrays.asList("姓名", "性别", "年龄", "专业");

        List<JSONArray> jsonArrayList = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();
        jsonArray.add("张三");
        jsonArray.add("男");
        jsonArray.add(18);
        jsonArray.add("相关专业");
        jsonArrayList.add(jsonArray);
        JSONArray jsonArray1 = new JSONArray();
        jsonArray1.add("李四");
        jsonArray1.add("女");
        jsonArray1.add(21);
        jsonArray1.add("会计专业");
        jsonArrayList.add(jsonArray1);
        JSONArray jsonArray2 = new JSONArray();
        jsonArray2.add("王五");
        jsonArray2.add("男");
        jsonArray2.add(26);
        jsonArray2.add("软件专业");
        jsonArrayList.add(jsonArray2);
        JSONArray jsonArray3;
        for (int i = 0; i < 90; i++) {
        //for (int i = 1; i < 13; i++) {
            jsonArray3 = new JSONArray();
            jsonArray3.add("王五" + i);
            jsonArray3.add("男" + i);
            jsonArray3.add(26 + i);
            jsonArray3.add("软件专业软件专业" + i);
            jsonArrayList.add(jsonArray3);
        }

        tableParamMap.put("table1", new TableParam(4, "", titleList, jsonArrayList));
        allData.put("tableMap", tableParamMap);
        return allData;
    }

    /**
     * 替换文本
     *
     * @param keyMap    - 入参集合
     * @param textValid - 是否强校验
     */
    public static void testText(Map<String, Object> keyMap, Boolean textValid) {
        System.out.println("keyMap");
        System.out.println("keyMap = " + keyMap);
        for (XSLFSlide slide : pptUtil.getSlides()) {
            // 获取该幻灯片内的所有段落
            List<XSLFTextParagraph> paragraphs = pptUtil.getParagraphsFromSlide(slide);
            for (XSLFTextParagraph paragraph : paragraphs) {
                if (textValid
                        && !"CT_TextParagraph".equals(paragraph.getXmlObject()
                        .schemaType().getName().getLocalPart())) {
                    continue;
                }
                // 如果该段落文本不为空，则打印文本
                if (!"".equals(paragraph.getText())) {
                    String localPart = paragraph.getXmlObject().schemaType().getName().getLocalPart();
                    System.out.println(localPart);
                    System.out.println(paragraph.getText());
                }
                // 对该段落中所有标签 {**} 进行替换
                pptUtil.replaceTagInParagraph(paragraph, keyMap);
            }
        }

    }

    /**
     * 渲染table
     *
     * @param tableParamMap - table Map数据
     */
    private static void testTable(Map<String, TableParam> tableParamMap) {
        // 每一个幻灯片
        for (int slideIndex = 0; slideIndex < pptUtil.getSlides().size(); slideIndex++) {
            System.out.println("第" + (slideIndex + 1) + "个幻灯片");
            XSLFSlide slide = pptUtil.getSlides().get(slideIndex);

            List<XSLFTable> allTableFromSlide = pptUtil.getAllTableFromSlide(slide);
            for (int tableIndex = 0; tableIndex < allTableFromSlide.size(); tableIndex++) {
                System.out.println("第" + (tableIndex + 1) + "个table");
                XSLFTable xslfTable = allTableFromSlide.get(tableIndex);

                XSLFTableRow tableFirstRow = pptUtil.getTableRow(xslfTable, 1);
                XSLFTableCell tableFirstCol = pptUtil.getTableCol(tableFirstRow, 0);
                String tableName = tableFirstCol.getText() != null
                        ? tableFirstCol.getText().replace("${", "")
                        .replace("}", "") : "";

                if (!tableParamMap.containsKey(tableName)) {
                    continue;
                }

                List<JSONArray> tableData = tableParamMap.get(tableName).getTableData();
                List<XSLFSlide> xslfSlideList = new ArrayList<>();
                xslfSlideList.add(slide);
                while (true) {
                    slideIndex++;
                    // 备份原始表格
                    xslfSlideList.add(pptUtil.getPptx()
                            .createSlide(slide.getMasterSheet())
                            .importContent(xslfSlideList.get(xslfSlideList.size() - 1)));
                    XSLFSlide slideNew = xslfSlideList.get(xslfSlideList.size() - 1);
                    pptUtil.getPptx().setSlideOrder(slideNew, slideIndex);

                    Integer currentDataIndex = rendTable(xslfTable, tableData);
                    if (currentDataIndex != -1) {
                        xslfTable = findTable(slideNew, tableName);
                        if (xslfTable == null) {
                            pptUtil.getPptx().removeSlide(slideIndex);
                            slideIndex--;
                            break;
                        }
                        tableData = tableData.subList(currentDataIndex, tableData.size());
                    } else {
                        pptUtil.getPptx().removeSlide(slideIndex);
                        slideIndex--;
                        break;
                    }
                }
                System.out.println("该表格渲染完毕");
            }
        }
    }


    /**
     * 生成表格第一页
     *
     * @param xslfTable  - 表格对象
     * @param tableData - 表格参数
     * @return currentDataIndex - 是否需要分页，-1不需要分页
     */
    private static Integer rendTable(XSLFTable xslfTable, List<JSONArray> tableData) {
        // 大小、位置信息
        Dimension pageSize = pptUtil.getPptx().getPageSize();
        System.out.println("幻灯片高度：" + pageSize.height);
        System.out.println("幻灯片宽度：" + pageSize.width);
        Rectangle2D anchorTable = xslfTable.getAnchor();
        double maxTableHeight = pageSize.height - 2 * anchorTable.getY();
        double tableHeight = anchorTable.getHeight();
        System.out.println("初始表格位置：" + anchorTable.getY());
        System.out.println("初始表格高度：" + tableHeight + "\r\n");
        System.out.println("表格最大高度：" + maxTableHeight + "\r\n");


        int rows = pptUtil.getTableRowsNum(xslfTable);
        int cols = pptUtil.getTableColsNum(xslfTable);
        System.out.println(String.format("%s行%s列\r\n", rows, cols));

        XSLFTableRow xslfTableRowFisrt = xslfTable.getRows().get(1);
        XSLFTableCell xslfTableCell = xslfTableRowFisrt.getCells().get(0);

        Color borderColor = xslfTableCell.getBorderColor(TableCell.BorderEdge.bottom);
        borderColor = borderColor == null ? new Color(255, 255, 255) : borderColor;
        Double borderWidth = xslfTableCell.getBorderWidth(TableCell.BorderEdge.right);
        borderWidth = borderWidth == null ? 1 : borderWidth;

        XSLFTableRow tableRow;
        XSLFTableCell tableCol;
        for (int dataIndex = 0; dataIndex < tableData.size(); dataIndex++) {
            JSONArray colJsonArray = tableData.get(dataIndex);

            double maxColHeight = 0;
            if (dataIndex == 0) {
                tableRow = pptUtil.getTableRow(xslfTable, 1);
                for (int cellIndex1 = 0; cellIndex1 < tableRow.getCells().size(); cellIndex1++) {
                    tableCol = tableRow.getCells().get(cellIndex1);
                    String value = String.valueOf(colJsonArray.get(cellIndex1));
                    pptUtil.setCellText(tableCol, value);

                    VerticalAlignment verticalAlignment = tableCol.getVerticalAlignment();

                    CTTextParagraph xmlObject = tableCol.getTextParagraphs().get(0).getXmlObject();

                    /*XSLFTextParagraph xslfTextParagraph =
                            tableCol.getTextParagraphs().isEmpty() ?
                                    tableCol.addNewTextParagraph()
                                    : tableCol.getTextParagraphs().get(0);
                    xslfTextParagraph.setTextAlign(TextParagraph.TextAlign.CENTER);
                    xslfTextParagraph.addNewTextRun().setText(value);
                    xslfTextParagraph.setFontAlign(TextParagraph.FontAlign.CENTER);
                    tableCol.setVerticalAlignment(VerticalAlignment.MIDDLE);*/

                    /*if (tableCol.getTextHeight() > maxColHeight) {
                        maxColHeight = tableCol.getAnchor().getHeight() * rate;
                    }*/
                    /*System.out.println("当前行高度：" + xslfTable.getRowHeight(1));
                    System.out.println("当前列高度：" + tableCol.getAnchor().getHeight() + "文本框高度：");*/
                    // System.out.printf("第%s行,第%s列：%s%n", dataIndex + 2, cellIndex1 + 1, xslfTextParagraph.getText());
                }
            } else {
                tableRow = xslfTable.addRow();
                for (int cellIndex2 = 0; cellIndex2 < cols; cellIndex2++) {
                     //tableCol = tableRow.addCell();
                    tableCol = tableRow.getCells().get(cellIndex2);

                    XSLFTableCell xslfTableCellFirst = xslfTableRowFisrt.getCells().get(cellIndex2);


                    // 垂直方向
                    // tableCol.setVerticalAlignment(xslfTableCellFirst.getVerticalAlignment());
                    // 强制垂直居中
                     tableCol.setVerticalAlignment(VerticalAlignment.MIDDLE);
                    //pptUtil.setCellVerticalAlign(tableCol, "bottom");

                    /*CTTableCell xmlObject = (CTTableCell) tableCol.getXmlObject();

                    int ordinal = xslfTableCellFirst.getVerticalAlignment().ordinal();
                    STTextAnchoringType.Enum anEnum = STTextAnchoringType.Enum.forInt(ordinal + 1);
                    xmlObject.getTcPr().setAnchor(anEnum);*/

                    // VerticalAlignment verticalAlignment = tableCol.getVerticalAlignment();


                    Rectangle2D anchorFirst = xslfTableCellFirst.getAnchor();
                    Rectangle2D anchor1 = tableCol.getAnchor();
                    anchor1.setRect(anchor1.getX(), anchor1.getY(), anchorFirst.getWidth(), anchorFirst.getHeight());
                    tableCol.setAnchor(anchor1);
                    String value = String.valueOf(colJsonArray.get(cellIndex2));

                    // 设置边框颜色 默认白色
                    pptUtil.setCellBorderColor(xslfTableCellFirst, tableCol, new Color(255, 255, 255));
                    // 设置边框宽度，默认为1
                    pptUtil.setCellBorderWidth(xslfTableCellFirst, tableCol, 1d);
                    // 不是默认颜色就设置颜色
                    Color fillColor = xslfTableCellFirst.getFillColor();
                    if (!new Color(149, 179, 215).equals(fillColor)) {
                        tableCol.setFillColor(xslfTableCellFirst.getFillColor());
                    }

                    XSLFTextParagraph xslfTextParagraph = tableCol.addNewTextParagraph();
                    // 水平
                    XSLFTextParagraph xslfTextParagraphFirst = xslfTableCellFirst.getTextParagraphs().get(0);
                    xslfTextParagraph.setTextAlign(xslfTextParagraphFirst.getTextAlign());
                    // xslfTextParagraph.setFontAlign(xslfTextParagraphFirst.getFontAlign());

                    // xslfTextParagraph.addNewTextRun().setText(value);

                     pptUtil.setCellText(tableCol, value);

                    //System.out.println("当前列高度：" + tableCol.getAnchor().getHeight() + "文本框高度：" + xslfTextParagraph.getParentShape().getTextHeight());
                    double curTableY = anchor1.getY() + anchor1.getHeight();
                    System.out.println("当前单元格底边：" + curTableY);
                    if (curTableY > maxColHeight) {
                        maxColHeight = curTableY;
                    }
                    // System.out.println("当前列高度：" + tableCol.getTextHeight());
                    // System.out.println("当前表格高度：" + xslfTable.getAnchor().getHeight());

                }
            }
            tableHeight += maxColHeight;
            System.out.println("最大表格高度：" + maxTableHeight);
            //System.out.println("当前表格高度：" + xslfTable.getAnchor().getHeight());
            System.out.println("当前表格高度2：" + maxColHeight);
            /*System.out.println("当前行结束\r\n\r\n");*/

            if (maxColHeight > maxTableHeight) {
                // 需要换页
                System.out.println("需要换页" + (dataIndex + 1));
                xslfTable.removeRow(xslfTable.getRows().size() - 1);
                xslfTable.removeRow(xslfTable.getRows().size() - 1);

                System.out.println("最终表格高度：" + xslfTable.getAnchor().getHeight());

                return dataIndex - 1;
            }
        }
        return -1;
    }

    private static XSLFTable findTable (XSLFSlide slide, String tableName1) {
        List<XSLFTable> allTableFromSlide = pptUtil.getAllTableFromSlide(slide);
        for (int tableIndex = 0; tableIndex < allTableFromSlide.size(); tableIndex++) {
            System.out.println("第" + (tableIndex + 1) + "个table");
            XSLFTable xslfTable = allTableFromSlide.get(tableIndex);

            XSLFTableRow tableFirstRow = pptUtil.getTableRow(xslfTable, 1);
            XSLFTableCell tableFirstCol = pptUtil.getTableCol(tableFirstRow, 0);
            String tableName = tableFirstCol.getText() != null
                    ? tableFirstCol.getText().replace("${", "")
                    .replace("}", "") : "";

            if (tableName1.equals(tableName)) {
                return xslfTable;
            }
        }
        return null;
    }


    /**
     * 渲染table 测试方法
     *
     * @param tableParamList - table数据集合
     */
    private static void testTableTest(List<TableParam> tableParamList) {
        XSLFSlide slide = pptUtil.getSlides().get(4);
        List<XSLFTable> allTableFromSlide = pptUtil.getAllTableFromSlide(slide);

        for (XSLFTable xslfTableRows : allTableFromSlide) {
            XSLFTableRow xslfTableCells = xslfTableRows.getRows().get(1);
            System.out.println(xslfTableCells.getCells().get(0).getText());
            xslfTableCells.getCells().get(0).setText("李四");

            // 生成第一行
            XSLFTableRow firstRow = xslfTableRows.addRow();
            // 生成第一个单元格
            XSLFTableCell firstCell = firstRow.addCell();

            /** 设置单元格的边框颜色 **//*
            firstCell.setBorderColor(TableCell.BorderEdge.top, new Color(10,100,120));
            firstCell.setBorderColor(TableCell.BorderEdge.right, new Color(10,100,120));
            firstCell.setBorderColor(TableCell.BorderEdge.bottom, new Color(10,100,120));
            firstCell.setBorderColor(TableCell.BorderEdge.left, new Color(10,100,120));

            *//** 设置单元格边框 **//*
            firstCell.setBorderWidth(TableCell.BorderEdge.top, 2);
            firstCell.setBorderWidth(TableCell.BorderEdge.right,2);
            firstCell.setBorderWidth(TableCell.BorderEdge.bottom, 2);
            firstCell.setBorderWidth(TableCell.BorderEdge.left, 2);*/
            /** 设置文本 **/
            firstCell.setText("张三");


            firstCell.setShapeType(xslfTableCells.getCells().get(0).getShapeType());
            firstCell.setStrokeStyle(xslfTableCells.getCells().get(0).getStrokeStyle());
            firstCell.setHorizontalCentered(true);
            /** 设置单元格的边框宽度 **/


        }
    }


    public static void testLdt() {
        // pptUtil.getRadarChartFromChart();
        /*List<XSLFTable> allTableFromSlide = pptUtil.getAllTableFromSlide(slide);

        for (XSLFTable xslfTable : allTableFromSlide) {

        }*/
    }


    public static void test2() {
        List<String> stringList = new ArrayList<>();
        stringList.add("asdfasff");
        stringList.add("test中${");
        stringList.add("address");
        stringList.add("}我是后面的信息啊${test2}");
        int start = 1;
        int end = 3;

        int[] idx = {0};
        List<String> collect = stringList.stream().filter(e -> ((++idx[0]) > start && idx[0] < end)).collect(Collectors.toList());

        System.out.println(collect);

        for (int i = end - 1; i > start; i--) {
            System.out.println(stringList.get(i));
            stringList.remove(i);
        }

        System.out.println(stringList);

    }


    private static void test3() {
        JSONArray jsonArray = new JSONArray();
        jsonArray.add("张三");
        jsonArray.add("男");
        jsonArray.add(18);
        jsonArray.add("相关专业");
        System.out.println(jsonArray.size());
    }

}
